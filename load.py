import json


def take_part(path_to_file, n_percents, path_to_out):
    with open(path_to_file) as f:
        lines = f.readlines()
        num_strings = len(lines) * n_percents // 100
        with open(path_to_out, 'w') as out:
            for line in lines[:num_strings]:
                json.dump(line, out)


take_part('../1m_6ex_karel/train.json', 1, '1_percent_train.json')
take_part('../1m_6ex_karel/train.json', 3, '3_percent_train.json')
take_part('../1m_6ex_karel/train.json', 10, '10_percent_train.json')
